interface OperacionDeString {
    String operacion(String a, String b);
   }
   class Concatenador implements OperacionDeString {
    public String operacion( String a, String b) {
    return a+b;
    }
   }
   class Extractor implements OperacionDeString {
    public String operacion (String a, String b) {
    return a.replace(b,""); // elimina de a todo substring b.
    }
   }
   public class Main1 {
    public static void main (String [] args) {
    Concatenador concatena = new Concatenador(); // [1]
    Extractor extraiga = new Extractor(); // [2]
    System.out.println("“Hola concatenado con Jóvenes =”" + concatena.operacion("“Hola”","”Jóvenes”"));
    System.out.println("“ola de mar=”"+extraiga.operacion("“Hola”","”H”"));
   }}
   