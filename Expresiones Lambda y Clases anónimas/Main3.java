interface OperacionDeString {
    String operacion(String a, String b);
   }
   class Concatenador implements OperacionDeString {
    public String operacion( String a, String b) {
    return a+b;
    }
   }
   class Extractor implements OperacionDeString {
    public String operacion (String a, String b) {
    return a.replace(b,""); // elimina de a todo substring b.
    }
   }
   public class Main3 { //Expresiones lambda
    public static void main (String [] args) {
    OperacionDeString concatena = (a,b) -> a+b; // new OperacionDeString(){ String operacion(String a, String b);} //!Como esto es lo que en realidad hace, sabe lo que es a y b
    OperacionDeString extraiga = (a,b) -> a.replace(b,"");
    System.out.println("Hola concatenado con Jóvenes =" + concatena.operacion("Hola","Jóvenes"));
    System.out.println("ola de mar="+extraiga.operacion("Hola","H"));
   }}
   