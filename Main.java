import java.util.ArrayList;

public class Main {
    public static void main (String[] args) {
        Animal perro = new Perro("Jack","Guau");
        Animal gato = new Gato("Milinis","Miau");
        Animal a = new Animal("Puppi");

        System.out.println(perro instanceof Perro);    
        ArrayList<Animal> animales = new ArrayList<Animal>();
        animales.add(perro);
        animales.add(gato);
        /*ArrayList<Gato> gatos = new ArrayList<Gato>();
        
        for (Animal a :  animales){
            String string = (a.getClass().getName());
            if (a.getClass() == gato.getClass()){
                Gato g =(Gato)a;
                gatos.add(g);
            }
        }*/
       //! Demostracion que pasa cuando hay referencia clase padre con implementacion clase hija

        System.out.println(a.getName());
        //System.out.println((animales.get(0).ladrido); Esto no se puede porque no podemos utilizar metodo de perro a una referencial de animal
        System.out.println(perro.getName()); //Segun ligado dinamico esto debe retorna el metodo de la clase hija aunque la referencia sea de la clase base
        Perro p = (Perro)(perro); //Si podemos recuperar info de perro cuando casteamos
        System.out.println(p.ladrido);
        System.out.println(p.getName());
        //System.out.println(gato.maullido);
        Gato g = (Gato)gato;
        System.out.println(g.maullido);


    }
}
class Animal {
    public String nombre;
    Animal(String n){
        nombre = n;
    }
    public String getName(){
        return nombre;
    }
}
class Perro extends Animal {
    public String ladrido;
    Perro(String n, String l){
        super(n);
        ladrido = l;
    }
    public String getName(){
        return "Perro " + nombre;
    }
}
class Gato extends Animal {
    public String maullido;
    Gato(String n, String m){
        super(n);
        maullido = m;
    }
    public String getName(){
        return "Gato " + nombre;
    }
}
